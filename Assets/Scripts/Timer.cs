﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using System;
using System.Diagnostics;
using UnityEngine.UI;

// using Debug=UnityEngine.Debug;

public class Timer : MonoBehaviour 
{	
	public static Timer t;

	public Text txtTimer;
	private bool isTxtUITimerSet = true;

	public Stopwatch time = new Stopwatch();
	private bool isRunning = false;

	void Start () 
	{
		if (t == null)
			t = this.GetComponent<Timer>();

		if( txtTimer == null )
		{
			print( "Ne postoji UI Text za tajmer!" );
			isTxtUITimerSet = false;
		}

	}
	void Update () 
	{
		/*
		if( isRunning )
		{
			TimeSpan ts = time.Elapsed;
			//string finalTime = ts.Minutes + ":" + ts.Seconds + ":" + ts.Milliseconds;
			print( ts.ToString().Substring( 0, 11 ) );		
		}
		else
		{
			print( "00:00:00.00" );		
		}
		*/
		UpdateUI();
	}

	public void StartTime()
	{
		//print( "Start" );
		time.Reset();
		time.Start();
		isRunning = true;
	}

	public void StopTime()
	{
		//print( "Stop" );
		time.Stop();
		isRunning = false;
		//TimeSpan ts = time.Elapsed;
		//print( ts );
	}

	public string GetCurrentTime ()
	{
		TimeSpan ts = time.Elapsed;
		string finalTime;

		/** probaj kakav je string. Ako stoperica nije startovana, izlazu fale milisekunde ( nije cak ni 000 ), pa onda na pocetku baca exception **/
		try
		{
			finalTime = ts.ToString().Substring( 3, 8 );
		}
		catch ( Exception e )
		{
			finalTime = "00:00.00";
		}
		return finalTime;
	}

	public void UpdateUI()
	{
		if( isTxtUITimerSet )
		{
			txtTimer.text = GetCurrentTime();
		}
	}
}
