﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIControl : MonoBehaviour 
{
	public Button btnSingleStandard;

	void Start () 
	{
		btnSingleStandard.onClick.AddListener( DoButtonSingleStandard );
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	/***** BUTTON EVENTS ****************************************************************************************/
	void DoButtonSingleStandard ()
	{
		StartSingleStandard();
	}
		
	/***** FUNC METHODS ****************************************************************************************/
	void StartSingleStandard()
	{
		SceneManager.LoadScene( "track_standard_01" );
	}
}
