﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameManagerImpulse : MonoBehaviour 
{
	public static GameManagerImpulse gm;

	private int moveCount = 0;

	/** razne promenljive za igru **/
	[Tooltip("Total checkpoints on this track")]
	public int totalCheckPoints = 0;              // ovu vrednost ce preuzeti auto.

	/** UI definicije **/
	public Text txtPushCount;

	void Start () 
	{
		if (gm == null)
			gm = this.GetComponent<GameManagerImpulse>();		

		//StopWatch.sw.UpdateDisplay();

		UpdateUI();
	}

	public int MoveCount 
	{
		get
		{
			return moveCount;
		}
		set
		{
			moveCount = value;
		}
	}

	void Update () {
		
	}

	public void UpdateUI ()
	{
		if( txtPushCount != null )
		{
			txtPushCount.text = "Total Moves: " + moveCount;
		}
	}
}