﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointScript : MonoBehaviour 
{
	public bool _isActive = true;

	void Start () 
	{
		
	}

	void Update () 
	{
		
	}

	public bool IsActive()
	{
		return _isActive;
	}

	public void Deactivate()
	{
		_isActive = false;
	}
}
