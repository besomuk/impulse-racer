﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour 
{
	public GameObject target;
	public float yOffset = 0;
	public float xOffset = 0;
	public float zOffset = 0;

	Transform _transform;

	float start_x, start_z, start_y;

	// Use this for initialization
	void Start () 
	{
		_transform = GetComponent<Transform> (); 	
		start_x = _transform.position.x;
		start_y = _transform.position.y;
		start_z = _transform.position.z;
	}
	
	// Update is called once per frame
	void Update () 
	{
		_transform.position = new Vector3 ( target.transform.position.x + xOffset, target.transform.position.y + yOffset, target.transform.position.z + zOffset);
	}
}
