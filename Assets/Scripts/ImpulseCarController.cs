﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImpulseCarController : MonoBehaviour {

	public Text txtPower;

	public GameObject Arrow;

	[Tooltip ("Maksimalna sila kojom je moguce gurnuti autic u napred")]
	public float maxPushForce = 1000;      // Maksimalna sila kojom je moguce gurnuti autic u napred
	[Tooltip ("Maksimalna sila kojom je moguce zarotirati autic")]
	public float maxRotationForce = 1000;  // Maksimalna sila kojom je moguce zarotirati autic
	[Tooltip ("Sila kocenja autica")]
	public float brakingPower = 1000;      // Sila kocenja autica

	Rigidbody _rigidbody;

	float _currentVelocity;          // trenutna brzina autica
	bool _isMoving = false;          // indikator kretanja
	int _checkPointCount = 0;        // broj prodjenih checkpointa
	float _currentPushForce = 0;     // trenutna sila kojom se gura autic
	float _currentRotationForce = 0; // trenutna sila rotacije autica

	//float temp = 1;

	public List<Axels> axels;
	[System.Serializable]
	public struct Axels
	{
		public WheelCollider leftWheel;
		public WheelCollider rightWheel;
		public bool motor;    // is this wheel attached to motor?
		public bool steering; // does this wheel apply steer angle?
		public bool braking;  // does it brake
	}

	private enum RotationDirection
	{
		LEFT,
		RIGHT
	}

	void Start () 
	{
		_rigidbody = GetComponent<Rigidbody>();
		UpdateUI();


	}

	// Update is called once per frame
	void FixedUpdate ()
	{
		/** DA LI SE KRECEMO? **/
		if( _rigidbody.velocity.magnitude > -0.005f && _rigidbody.velocity.magnitude < 0.005f )
			_isMoving = false;
		else
			_isMoving = true;


		if( !_isMoving )
		{
			/** URADI NAPRED **/
			if( Input.GetKey( KeyCode.W ) )
			{				
				_currentPushForce+=10;
				//StartCoroutine( DoArrows() );

				if( _currentPushForce > maxPushForce )
				{
					PushForward();
				}
				UpdateUI();
			}
			if( Input.GetKeyUp( KeyCode.W ) )
			{
				PushForward();
			}

			/** URADI DESNO **/
			if( Input.GetKey( KeyCode.D ) )
			{
				_currentRotationForce += 5;
				if( _currentRotationForce > maxRotationForce )
				{
					PushRotate( RotationDirection.RIGHT );
				}
				UpdateUI();
			}

			if( Input.GetKeyUp( KeyCode.D ) )
			{
				PushRotate( RotationDirection.RIGHT );
			}

			/** URADI LEVO **/
			if( Input.GetKey( KeyCode.A ) )
			{
				_currentRotationForce += 5;
				if( _currentRotationForce > maxRotationForce )
				{
					PushRotate( RotationDirection.LEFT );
				}
				UpdateUI();
			}

			if( Input.GetKeyUp( KeyCode.A ) )
			{
				PushRotate( RotationDirection.LEFT );
			}

			GameManagerImpulse.gm.UpdateUI();
		}
	}

	/// <summary>
	/// Simula silu kuckanja automobila u napred
	/// </summary>
	void PushForward()
	{
		axels[0].leftWheel.brakeTorque = 0;      // da bi se autic lepo kretao unapred, par stvari mora biti uradjeno kako treba...
		axels[0].rightWheel.brakeTorque = 0;     // prvo, moramo iskljuciti kocnice...		 
		axels[0].leftWheel.motorTorque = 10;     // a onda moramo dati neki minimalni impuls motoru, jer je wheel collider inace u nekoj 
		axels[0].rightWheel.motorTorque = 10;    // vrsti 'autobrake' moda		

		_rigidbody.AddForce( transform.forward * _currentPushForce * _rigidbody.mass);  // iskljucili smo autobrake, daj mu silu
		_currentPushForce = 0;                                                          // resetuj silu
		StartCoroutine( WaitForLittleBit( "AutoBrake", 0.1f ) );                        // ukljuci automatsku kocnicu nakon nekog vremena

		GameManagerImpulse.gm.MoveCount++;
	}

	/// <summary>
	/// Automatska kocnica
	/// </summary>
	void AutoBrake()
	{
		axels[0].leftWheel.motorTorque  = 0;            // iskljuci motor na levom tocku
		axels[0].rightWheel.motorTorque = 0;            // iskljuci motor na desnom tocku
		axels[0].leftWheel.brakeTorque  = brakingPower; // ukljuci kocnice
		axels[0].rightWheel.brakeTorque = brakingPower;		
	}

	/// <summary>
	/// Crta strelice...
	/// </summary>
	/// <returns>The arrows.</returns>
	IEnumerator DoArrows()
	{
		float zpos = this.gameObject.transform.position.z;

		for ( int i = 0; i<9; i++)
		{
			zpos = zpos + 4;

			CreateArrow( new Vector3 ( this.transform.position.x, this.transform.position.y, zpos) );

			yield return new WaitForSeconds( 1 );
		}
	}

	/// <summary>
	/// Metoda za cekanje necega. Ulaz je duzina pauze ( float ) i ime metode ( string ) koja ce biti pozvana nakon te pauze.
	/// PAZNJA! Pozvati je iz StartCoroutine()!
	/// </summary>
	/// <returns>The for little bit.</returns>
	/// <param name="methodToCall">ime metode koja ce biti pozvana nakon pauze</param>
	/// <param name="timeToWait">duzina trajanja pauze</param>
	IEnumerator WaitForLittleBit ( string methodToCall, float timeToWait )
	{
		yield return new WaitForSeconds( timeToWait );
		Invoke( methodToCall, 0 );
	}

	float timeLeft = 1;

	void CreateArrow ( Vector3 pos )
	{
		GameObject go;
		go = Instantiate (Arrow, pos, Quaternion.identity);    // napravi ga na mestu gde je spawn objekat
		go.transform.eulerAngles = new Vector3 ( 0, -180, 0);		
		go.name = "New Arrow";
	}

	/// <summary>
	/// Simulira kuckanje automobila u stranu
	/// </summary>
	/// <param name="dir">Dir.</param>
	void PushRotate( RotationDirection dir )
	{
		DisableWheelColliders();		
		if( dir == RotationDirection.LEFT )
		{
			_rigidbody.AddTorque( new Vector3( 0, 1, 0 ) * -_currentRotationForce * _rigidbody.mass  );
		}

		if( dir == RotationDirection.RIGHT )
		{
			_rigidbody.AddTorque( new Vector3( 0, 1, 0 ) * _currentRotationForce  * _rigidbody.mass  );
		}
		_currentRotationForce = 0;
		GameManagerImpulse.gm.MoveCount++;
		StartCoroutine( WaitForLittleBit( "EnableWheelColliders", 0.1f ) );
	}

	void DisableWheelColliders ()
	{
		axels[0].leftWheel.enabled = false;
		axels[0].rightWheel.enabled = false;
		axels[1].leftWheel.enabled = false;
		axels[1].rightWheel.enabled = false;		
		/*
		SphereCollider s_FL = axels[0].leftWheel.gameObject.AddComponent<SphereCollider>();
		SphereCollider s_FR = axels[0].rightWheel.gameObject.AddComponent<SphereCollider>();
		SphereCollider s_RL = axels[1].leftWheel.gameObject.AddComponent<SphereCollider>();
		SphereCollider s_RR = axels[1].rightWheel.gameObject.AddComponent<SphereCollider>();
		s_FL.center = new Vector3( 0, -0.27f, 0 );
		s_FR.center = new Vector3( 0, -0.27f, 0 );
		s_RL.center = new Vector3( 0, -0.27f, 0 );
		s_RR.center = new Vector3( 0, -0.27f, 0 );
		*/
	}

	void EnableWheelColliders ()
	{
		print( "vracam wheel collidere" );
		axels[0].leftWheel.enabled = true;
		axels[0].rightWheel.enabled = true;
		axels[1].leftWheel.enabled = true;
		axels[1].rightWheel.enabled = true;		
	}

	void UpdateUI()
	{
		txtPower.text = "UP: " + _currentPushForce + "\n" + "ROT: " + _currentRotationForce;
	}

	/** da li smo pogodili checkpoint **/
	void OnTriggerEnter ( Collider col )
	{
		if( col.gameObject.tag == "CheckPoint" && col.gameObject.GetComponent<CheckPointScript>().IsActive() )
		{
			_checkPointCount++;

			col.gameObject.GetComponent<CheckPointScript>().Deactivate();
		}

		if( _checkPointCount == GameManagerImpulse.gm.totalCheckPoints )
			print( "GOTOVO, trka je zavrsena...." );
	}
}
