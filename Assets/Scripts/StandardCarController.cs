﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandardCarController : MonoBehaviour 
{
	public float maxMotorTorque   = 2000;
	public float maxSteeringAngle = 45;
	public float brakingPower     = 5000;

	bool isBrake = false;

	Rigidbody _rigidbody;

	int _checkPointCount = 0;

	public List<Axels> axels;
	[System.Serializable]
	public struct Axels
	{
		public WheelCollider leftWheel;
		public WheelCollider rightWheel;
		public bool motor;     // is this wheel attached to motor?
		public bool steering;  // does this wheel apply steer angle?
		public bool brake;     // does it brake
	}

	void Start ()
	{
		_rigidbody = GetComponent<Rigidbody>();		
		_rigidbody.centerOfMass = new Vector3( 0, 0, 0 );


	}		
	

	void Update ()
	{
		
	}

	public void ApplyLocalPositionToVisuals(WheelCollider collider)
	{
		if (collider.transform.childCount == 0)
		{
			return;
		}

		Transform visualWheel = collider.transform.GetChild(0);

		Vector3 position;
		Quaternion rotation;
		collider.GetWorldPose(out position, out rotation);

		visualWheel.transform.position = position;
		visualWheel.transform.rotation = rotation;
	}

	public void FixedUpdate()
	{
		float motor = maxMotorTorque * Input.GetAxis("Vertical");
		float steering = maxSteeringAngle * Input.GetAxis("Horizontal");

		if( Input.anyKeyDown && !GameManagerStandard.gm.IsRunning )
		{
			GameManagerStandard.gm.IsRunning = true;
			Timer.t.StartTime();
		}

			

		if( Input.GetKey( KeyCode.Space ) )
		{
			isBrake = true;
		}
		else
		{
			isBrake = false;
		}

		foreach (Axels axleInfo in axels)
		{
			if (axleInfo.steering)
			{
				axleInfo.leftWheel.steerAngle = steering;
				axleInfo.rightWheel.steerAngle = steering;
			}

			if (axleInfo.motor)
			{				
				if( isBrake  && axleInfo.brake )
				{					
					axleInfo.leftWheel.brakeTorque = brakingPower;
					axleInfo.rightWheel.brakeTorque = brakingPower;
					axleInfo.leftWheel.motorTorque = 0;
					axleInfo.rightWheel.motorTorque = 0;
				}
				else
				{
					axleInfo.leftWheel.brakeTorque = 0;
					axleInfo.rightWheel.brakeTorque = 0;					
					axleInfo.leftWheel.motorTorque = motor;
					axleInfo.rightWheel.motorTorque = motor;
				}
			}
				
			ApplyLocalPositionToVisuals(axleInfo.leftWheel);
			ApplyLocalPositionToVisuals(axleInfo.rightWheel);
		}

		//print( isBrake );
	}

	/** da li smo pogodili checkpoint **/
	void OnTriggerEnter ( Collider col )
	{
		/** proveravam obican checkpoint **/
		if( col.gameObject.tag == "CheckPoint"  && col.gameObject.GetComponent<CheckPointScript>().IsActive() )
		{			
			_checkPointCount++;                                           // povecaj broj prodjenih checkpointa
			col.gameObject.GetComponent<CheckPointScript>().Deactivate(); // deaktiviraj checkpoint kroz koji smo prosli
		}

		/** Specijalni slucaj, kada auto prolazi kroz finalni check point.
		 ** Da mi ne bi racunao prvi checkpointa na startu, aktiviram ga jedino kada je broj predjenih checkpointa > 0, tj.
		 ** kada je prosao ceo krug **/
		if( ( col.gameObject.tag == "CheckPointFinal" && _checkPointCount > 0 ) && col.gameObject.GetComponent<CheckPointScript>().IsActive() )
		{			
			_checkPointCount++;                                           // povecaj broj prodjenih checkpointa
			col.gameObject.GetComponent<CheckPointScript>().Deactivate(); // deaktiviraj checkpoint kroz koji smo prosli
		}


		if( col.gameObject.tag == "CheckPointFinal" && _checkPointCount == GameManagerStandard.gm.totalCheckPoints )
		{
			print( "GOTOVO, trka je zavrsena...." );
			Timer.t.StopTime();
		}
	}
}
	