﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameManagerStandard : MonoBehaviour 
{
	public static GameManagerStandard gm;

	private int moveCount = 0;

	/** razne promenljive za igru **/
	[Tooltip("Total checkpoints on this track")]
	public int totalCheckPoints = 0;              // ovu vrednost ce preuzeti auto.

	/** UI definicije **/
	public Text txtPushCount;

	private bool isRunning = false;

	void Start () 
	{
		if (gm == null)
			gm = this.GetComponent<GameManagerStandard>();		

		//StopWatch.sw.UpdateDisplay();

		UpdateUI();
	}

	public bool IsRunning
	{
		get
		{
			return isRunning;
		}
		set
		{
			isRunning = value;
		}
	}

	public int MoveCount 
	{
		get
		{
			return moveCount;
		}
		set
		{
			moveCount = value;
		}
	}

	void Update () {
		
	}

	public void UpdateUI ()
	{
		if( txtPushCount != null )
		{
			txtPushCount.text = "Total Moves: " + moveCount;
		}
	}
}