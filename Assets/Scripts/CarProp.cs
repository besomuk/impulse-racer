﻿/// <summary>
/// Car, but without control.
/// </summary>
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarProp : MonoBehaviour 
{
	Rigidbody _rigidbody;

	public List<Axels> axels;
	[System.Serializable]
	public struct Axels
	{
		public WheelCollider leftWheel;
		public WheelCollider rightWheel;
		//public bool motor;    // is this wheel attached to motor?
		//public bool steering; // does this wheel apply steer angle?
		//public bool braking;  // does it brake
	}

	void Start ()
	{
		_rigidbody = GetComponent<Rigidbody>();		
		_rigidbody.centerOfMass = new Vector3( 0, 0, 0 );
	}	

	public void ApplyLocalPositionToVisuals(WheelCollider collider)
	{
		if (collider.transform.childCount == 0)
		{
			return;
		}

		Transform visualWheel = collider.transform.GetChild(0);

		Vector3 position;
		Quaternion rotation;
		collider.GetWorldPose(out position, out rotation);

		visualWheel.transform.position = position;
		visualWheel.transform.rotation = rotation;
	}

	public void FixedUpdate()
	{
		foreach (Axels axleInfo in axels)
		{
			ApplyLocalPositionToVisuals(axleInfo.leftWheel);
			ApplyLocalPositionToVisuals(axleInfo.rightWheel);
		}
	}
}
